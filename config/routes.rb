Rails.application.routes.draw do
  get 'search', to: 'search#search'
  resources :unique_routes
  resources :users
  resources :sessions, only: [:new, :create, :destroy]  
  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
 # root 'sessions#new'
  root 'home#index'

end
